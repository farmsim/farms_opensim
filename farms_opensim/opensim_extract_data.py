""" Script to extract mouse data from opensim. """

import os

def plot_and_export(plot_name, y_axis, muscle, x_axis, export_dir):
    """
 
    Parameters
    ----------
    plot_name : <str>
        Name of the plot table
    y_axis : <str>
        Name of the y_axis quantity
    muscle : <str>
        Name of the muscle    
    x_axis : <str>
        Name of the x_axis quantity
    export_dir : <str>
        Path to the export directory

    Returns
    -------
    out : <bool>
        True if successful
    """
    #: Create plotter
    plotter = createPlotterPanel(plot_name)
    #: Add curve
    curve = addAnalysisCurve(
        plotter, y_axis, muscle, x_axis
    )
    #: Change axis lables
    plotter.setXAxisLabel(x_axis);
    plotter.setYAxisLabel(muscle + "-" + y_axis);
    #: export data
    file_name = muscle + "_" + x_axis + "_" + y_axis + ".txt"
    exportData(plotter, os.path.join(export_dir, file_name))

muscle_joint = [
    ['GEM', 'Hip_rotation'],
    ['GM_dorsal', 'Hip_rotation'],
    ['GM_mid', 'Hip_rotation'],
    ['GM_ventral', 'Hip_rotation'],
    ['OE', 'Hip_rotation'],
    ['OI', 'Hip_rotation'],
    ['QF', 'Hip_rotation'],
    ['BFA', 'Hip_flexion'],
    ['BFP_caudal', 'Hip_flexion'],
    ['BFP_cranial', 'Hip_flexion'],
    ['BFP_mid', 'Hip_flexion'],
    ['CF', 'Hip_flexion'],
    ['GA', 'Hip_flexion'],
    ['GM_dorsal', 'Hip_flexion'],
    ['GM_mid', 'Hip_flexion'],
    ['GM_ventral', 'Hip_flexion'],
    ['GP', 'Hip_flexion'],
    ['SM', 'Hip_flexion'],
    ['ST', 'Hip_flexion'],
    ['ILI', 'Hip_flexion'],
    ['PECT', 'Hip_flexion'],
    ['PMA', 'Hip_flexion'],
    ['PMI', 'Hip_flexion'],
    ['AB', 'Hip_adduction'],
    ['AL', 'Hip_adduction'],
    ['AM', 'Hip_adduction'],
    ['GA', 'Hip_adduction'],
    ['GP', 'Hip_adduction'],
    ['BFA', 'Knee_extension'],
    ['BFP_caudal', 'Knee_extension'],
    ['BFP_cranial', 'Knee_extension'],
    ['BFP_mid', 'Knee_extension'],
    ['GA', 'Knee_extension'],
    ['GP', 'Knee_extension'],
    ['LG', 'Knee_extension'],
    ['MG', 'Knee_extension'],
    ['PLANT', 'Knee_extension'],
    ['POP', 'Knee_extension'],
    ['SM', 'Knee_extension'],
    ['ST', 'Knee_extension'],
    ['RF', 'Knee_extension'],
    ['VI', 'Knee_extension'],
    ['VL', 'Knee_extension'],
    ['VM', 'Knee_extension'],
    ['EDL', 'Ankle_flexion'],
    ['EDH', 'Ankle_flexion'],
    ['TA', 'Ankle_flexion'],
    ['FDL', 'Ankle_flexion'],
    ['MG', 'Ankle_flexion'],
    ['LG', 'Ankle_flexion'],
    ['PB', 'Ankle_flexion'],
    ['PQDA', 'Ankle_flexion'],
    ['PDQI', 'Ankle_flexion'],
    ['PL', 'Ankle_flexion'],
    ['PLANT', 'Ankle_flexion'],
    ['PT', 'Ankle_flexion'],
    ['SOL', 'Ankle_flexion'],
    ['PB', 'Ankle_inversion'],
    ['PDQA', 'Ankle_inversion'],
    ['PDQI', 'Ankle_inversion'],
    ['PL', 'Ankle_inversion'],
    ['PT', 'Ankle_inversion']
]

#: Get the current model in the scene
model = getCurrentModel()

#: Joint coordinate name to index
joint_name_to_index = {}
for j in range(model.getCoordinateSet().getSize()):
    joint_name_to_index[str(model.getCoordinateSet().get(j))] = j

#: Muscle name to index
muscle_name_to_index = {}
for j in range(model.getMuscles().getSize()):
    muscle_name_to_index[str(model.getMuscles().get(j))] = j

#: Quantity to export plots
export_quantity = 'MomentArm'

#: Create export directory
root_dir = '/Users/tatarama/EPFL/DATA/MOUSE/HINDLIMB_OPENSIM_DATA/'
export_dir = os.path.join(root_dir, export_quantity.replace(' ', '-'))
os.mkdir(export_dir)

#: Loop over all the muscle-joint combinations
for mj in muscle_joint:
    if export_quantity in ['Moment', 'MomentArm']:
        special_export_quantity = export_quantity + "." + mj[1]
        plot_and_export(
            special_export_quantity, special_export_quantity, mj[0], mj[1], export_dir
        )
    else:
        plot_and_export(
            export_quantity, export_quantity, mj[0], mj[1], export_dir
        )
