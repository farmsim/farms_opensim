##
# OpenSim to farms model exporter utility functions. This was tested with
# OpenSim v4.0.
#
# author: Dimitar Stanev (dimitar.stanev@epfl.ch),
# author : Shravan Tata (shravantr@gmail.com) (maintainer)
import os
from pathlib import Path

import farms_pylog as pylog
import h5py
import numpy as np
import opensim
from tqdm import tqdm


SCRIPT_PATH = Path(__file__).parent.absolute()
HOME_ENV = os.getenv("HOME")
OPENSIM_GEOMETRY_PATHS = [
    f"{HOME_ENV}/fork/SCONE/resources/geometry",
    SCRIPT_PATH.joinpath("..", "opensim-models", "Geometry")
]
OPENSIM_GEOMETRY_PATHS.append(os.getenv("OPENSIM_HOME"))


def get_opensim_geometry_paths():
    """ Get the paths to look for opensim meshes in stl format.

    Parameters
    ----------
    None

    Parameters
    ----------
    out : <list>
        List containing paths to search for open geometries
    """
    return OPENSIM_GEOMETRY_PATHS


def add_opensim_geometry_path(geometry_path):
    """Add a new path to the list of geometry paths for opensim in stl format.

    Parameters
    ----------
    geometry_path: <str>
        Path containing opensim geometry

    Returns
    -------
    out: <None>
        None

    """

    # Check for path validity
    if not os.path.isdir(os.path.abspath(geometry_path)):
        pylog.error(
            "Provided path is not a valid directory - {}".format(geometry_path))
        raise ValueError

    OPENSIM_GEOMETRY_PATHS.append(os.path.abspath(geometry_path))


def get_osim_array(array_type):
    """Get osim array

    Parameters
    ----------
    array_type : <str>
        Name of the array type

    Returns
    -------
    out : <opensim.Array(Type)>
        Opensim array of the required type
    """
    return getattr(opensim, array_type.title())


def read_osim(file_path):
    """Read an opensim file from *.osim.

    Parameters
    ----------
    file_path : <str>
         Path to the opensim osim file

    Returns
    -------
    out : <opensim.Model>
        opensim model
    """
    return opensim.Model(file_path)


def setup_osim_model(osim_file):
    """ Setup the osim model.

    Parameters
    ----------
    osim_file : <str>
        Path to the opensim file

    Returns
    -------
    model : <opensim.Model>
        Opensim model
    state : <opensim.State>
        State of the opensim model
    """

    add_opensim_geometry_path(os.path.split(osim_file)[0])
    model = read_osim(osim_file)
    state = model.initSystem()
    reset_model_to_zero_pose(model, state)
    return model, state


def disable_all_muscles(model, state):
    """Disable all muscles in the model."""
    for muscle in model.getMuscles():
        muscle.set_appliesForce(False)
        muscle.setAppliesForce(state, False)


def enable_all_muscles(model, state):
    """Disable all muscles in the model."""
    for muscle in model.getMuscles():
        muscle.set_appliesForce(True)
        muscle.setAppliesForce(state, True)


def enable_muscle(model, state, muscle_name):
    """Enable muscle force

    Parameters
    ----------
    model :

    state :


    Returns
    -------
    out :

    """
    for muscle in model.getMuscles():
        if muscle.getName() == muscle_name:
            muscle.set_appliesForce(True)
            muscle.setAppliesForce(state, True)


def save_model(model, file_path):
    """Save osim model

    Parameters
    ----------
    file_path :


    Returns
    -------
    out :

    """
    with open(file_path, "w") as stream:
        stream.write(model.dump())


def reset_model_to_zero_pose(model, state, force_zero=True):
    """Reset the model to zero pose.  If the joint limits do not allow the model
    to be set to zero then the joint is set to the minimal angle.

    Parameters
    ----------
    model : <opensim.Model>
        Opensim model
    state : <opensim.State>
        State of the opensim model

    Returns
    -------
    out : <None>

    """
    if force_zero:
        for joint in model.getJointSet():
            for i in range(joint.getPropertyByName('coordinates').size()):
                temp_joint = joint.safeDownCast(joint)
                # spatial_transform = temp_joint.get_SpatialTransform()
                coord = joint.get_coordinates(i)
                if coord.getMotionType() == 1:
                    # Unclamp and unlock
                    coord.setLocked(state, False)
                    coord.set_locked(False)
                    coord.setClamped(state, False)
                    coord.set_clamped(False)
                    # Set the value to zero
                    coord.setValue(state, 0.0)
                    coord.set_default_value(0.0)
    q0 = opensim.Vector(state.getNQ(), 0.0)
    state.setQ(q0)
    model.realizePosition(state)


def update_model_pose(model, state, pose, set_velocity_to_zero=True):
    """Update the pose of the model with the specified coordinate values
    specificed in the pose dictionary.

    Parameters
    ----------
    model : <opensim.Model>
        Opensim model
    state : <opensim.State>
        Opensim state
    pose : <dict>
        coord-name:value

    Returns
    -------
    out : <None>
    """
    for joint in model.getJointSet():
        for i in range(joint.getPropertyByName('coordinates').size()):
            coord = joint.get_coordinates(i)
            if coord.getName() in pose:
                # Set the value to zero
                coord.setValue(state, pose[coord.getName()])
                coord.setSpeedValue(state, 0.0)
                model.realizePosition(state)


def vec_to_list(vec):
    """Converts an OpenSim Vec3/6 to a Python list.
    """
    return [vec.get(i) for i in range(vec.size())]


def mat_to_list(mat):
    """Converts an Simbody Matrix to a Python double list.
    """
    return [
        [mat.get(i, j) for j in range(mat.ncol())]
        for i in range(mat.nrow())
    ]


def extract_static_body_properties(model):
    """Returns a dictionary with the static body properties. This includes mass,
    inertia, center of mass and attached geometries.

    Parameters
    ----------
    model: OpenSim.Model

    Returns
    -------
    body_properties: dictionary

    """
    body_properties = {}
    for body in model.getBodySet():
        body_properties[body.getName()] = {
            'mass': body.get_mass(),
            'CoM': vec_to_list(body.get_mass_center()),
            'inertia': vec_to_list(body.get_inertia()),
            'geometries': []
        }
        for i in range(body.getPropertyByName('attached_geometry').size()):
            geometry = body.get_attached_geometry(i)
            mesh = opensim.Mesh.safeDownCast(geometry)
            body_properties[body.getName()]['geometries'].append({
                'file':  mesh.get_mesh_file(),
                'scale':  vec_to_list(mesh.get_scale_factors())
            })
            if mesh.getSocket('frame').getConnecteePath() != '..':
                raise NotImplementedError(
                    'geometry attached to different frame'
                )
    return body_properties


def extract_static_joint_properties(model):
    """Returns a dictionary with the static joint properties. This includes
    parent/child bodies and location/orientation in child/parent frames. For
    more information, visit the following link to understand the terminology of
    the different frames and how Simbody/OpenSim defines its joints:

    https://simbody.github.io/simbody-3.6-doxygen/api/classSimTK_1_1MobilizedBody.html#details

    The location and orientation in parent define the reference frame of the
    joint in the parent's reference coordinate system. The location and
    orientation in child define the reference frame of the joint in the child's
    reference coordinate system. The type of joint, which is not extracted by
    this function, defines the relative transformation between these two frames.

    Parameters
    ----------
    model: OpenSim.Model

    Returns
    -------
    joint_properties: dictionary

    """
    joint_properties = {}
    for joint in model.getJointSet():
        parent = opensim.PhysicalOffsetFrame.safeDownCast(
            joint.getParentFrame())
        child = opensim.PhysicalOffsetFrame.safeDownCast(joint.getChildFrame())
        joint_properties[joint.getName()] = {
            'parent': parent.getName().replace('_offset', ''),
            'location_in_parent': vec_to_list(parent.get_translation()),
            'orientation_in_parent': vec_to_list(parent.get_orientation()),
            'child': child.getName().replace('_offset', ''),
            'location_in_child': vec_to_list(child.get_translation()),
            'orientation_in_child': vec_to_list(child.get_orientation())
        }

    return joint_properties


def calculate_joint_transformations(model, state):
    """Calculates the joint transformations relating the parent and child
    local frames (see extract_static_joint_properties).

    Parameters
    ----------
    model: OpenSim.Model
    state: OpenSim.State

    Returns
    -------
    joint_transformations: dictionary

    """
    joint_transformations = {}
    for joint in model.getJointSet():
        parent = opensim.PhysicalOffsetFrame.safeDownCast(
            joint.getParentFrame())
        child = opensim.PhysicalOffsetFrame.safeDownCast(joint.getChildFrame())
        X_PC = child.findTransformBetween(state, parent)
        joint_transformations[joint.getName()] = {
            'location': vec_to_list(X_PC.p()),
            'orientation': mat_to_list(X_PC.R())
        }

    return joint_transformations


def calculate_absolute_body_transformations(model, state):
    """Calculates the orientation and location of the model bodies with respect
    to the ground frame for the current state.

    Parameters
    ----------
    model: OpenSim.Model
    state: OpenSim.State

    Returns
    -------
    body_transformations: dictionary

    """
    body_transformations = {}
    for body in model.getBodySet():
        frame = opensim.Frame.safeDownCast(body)
        X_BG = frame.getTransformInGround(state)

        body_transformations[body.getName()] = {
            'location': vec_to_list(X_BG.p()),
            'orientation': mat_to_list(X_BG.R())
        }

    return body_transformations


def extract_muscle_properties(model):
    """Extract muscle properties (max isometric force, optimal fiber length, max
    contraction velocity, tendon slack length, pennation angle*).

    * pennation angle defined at optimal fiber length

    Parameters
    ----------
    model: OpenSim.Model

    Returns
    -------
    muscle_properties: dictionary

    """
    muscle_properties = {}
    for muscle in model.getMuscles():
        muscle_properties[muscle.getName()] = {
            'max_isometric_force': muscle.getMaxIsometricForce(),
            'optimal_fiber_length': muscle.getOptimalFiberLength(),
            'max_contraction_velocity': muscle.getMaxContractionVelocity(),
            'tendon_slack_length': muscle.getTendonSlackLength(),
            'pennation_angle': muscle.getPennationAngleAtOptimalFiberLength()
        }

    return muscle_properties


def extract_muscle_path_point_properties(model):
    """Extract muscle path point properties (body, relative location in body
    frame and type).

    Parameters
    ----------
    model: OpenSim.Model

    Returns
    -------
    muscle_path_point_properties: dictionary

    """
    muscle_path_point_properties = {}
    for muscle in model.getMuscles():
        muscle_path_point_properties[muscle.getName()] = []
        geometry = muscle.getGeometryPath()
        for point in geometry.getPathPointSet():
            path_point = opensim.PathPoint.safeDownCast(point)
            conditional_point = opensim.ConditionalPathPoint.safeDownCast(
                point)
            moving_point = opensim.MovingPathPoint.safeDownCast(point)
            point_data = {}
            if path_point is not None:
                point_data = {
                    'body': path_point.getBodyName(),
                    'location': vec_to_list(path_point.get_location()),
                    'type:': 'fix_point'
                }

            if conditional_point is not None:
                point_data = {
                    'body': path_point.getBodyName(),
                    'location': vec_to_list(path_point.get_location()),
                    'type:': 'conditional_point'
                }
                print('Warning: Conditional point, not properly implemented.')

            if moving_point is not None:
                point_data = {
                    'type:': 'moving_point'
                }
                print('Warning: Moving point, not properly implemented.')

            muscle_path_point_properties[muscle.getName()].append(point_data)

    return muscle_path_point_properties


def calculate_absolute_muscle_path_point(model, state):
    """Extract muscle path point locations for the current state.

    Parameters
    ----------
    model: OpenSim.Model
    state: OpenSim.State

    Returns
    -------
    absolute_muscle_path_points: dictionary

    """
    absolute_muscle_path_points = {}
    for muscle in model.getMuscles():
        absolute_muscle_path_points[muscle.getName()] = []
        geometry = muscle.getGeometryPath()
        for point in geometry.getPathPointSet():
            absolute_muscle_path_points[muscle.getName()].append(
                vec_to_list(point.getLocation(state)))

    return absolute_muscle_path_points


def extract_static_coordinate_properties(model):
    """Extract the joint axes, limits and coordinate names.

    Parameters
    ----------
    model : <opensim.Model>
        Opensim model

    Returns
    -------
    coordinate_properties: dictionary
    """
    coordinate_properties = {}
    for joint in model.getJointSet():
        coordinate_properties[joint.getName()] = []
        for i in range(joint.getPropertyByName('coordinates').size()):
            joint_class = getattr(opensim, joint.getConcreteClassName())
            temp_joint = joint_class.safeDownCast(joint)
            coord = joint.get_coordinates(i)
            if temp_joint.getConcreteClassName() == "CustomJoint":
                spatial_transform = temp_joint.get_SpatialTransform()
                coordinate_names = spatial_transform.getCoordinateNames()
                axis = vec_to_list(spatial_transform.getTransformAxis(
                    coordinate_names.findIndex(coord.getName())).getAxis())
            else:
                if temp_joint is not None:
                    axis = [0, 0, 1]
                else:
                    raise RuntimeError('Only CustomJoint or PinJoint is supported')

            coordinate_properties[joint.getName()].append({
                "name": coord.getName(),
                "motion_type": coord.getMotionType(),
                "range": [coord.get_range(0), coord.get_range(1)],
                "axis": axis
            })

        # In case coordinate properties are empty, then this can be a
        # WeldJoint. In that case, create a fixed joint by setting
        # motion_type =-1 and limits [0, 0].
        if coordinate_properties[joint.getName()] == [] and temp_joint is not None:
            coordinate_properties[joint.getName()].append({
                "name": joint.getName(),
                "motion_type": -1,
                "range": [0, 0],
                "axis": [0, 0, 1]
            })

    return coordinate_properties


def create_default_muscle_states_dict(num_points, coords):
    """Create a dict to store muscle states
    """
    # Result dict
    muscle_states = {}
    muscle_states['joint_angles'] = dict.fromkeys(coords, np.zeros(num_points))
    # Length
    muscle_states['fiber_length'] = np.zeros(num_points)
    muscle_states['tendon_length'] = np.zeros(num_points)
    muscle_states['muscle_tendon_length'] = np.zeros(num_points)
    muscle_states['pennation_angle'] = np.zeros(num_points)
    # Forces
    muscle_states['active_fiber_force'] = np.zeros(num_points)
    muscle_states['passive_fiber_force'] = np.zeros(num_points)
    muscle_states['tendon_force'] = np.zeros(num_points)
    # Moment
    muscle_states['moment_arm'] = dict.fromkeys(coords, np.zeros(num_points))
    muscle_states['moment'] = dict.fromkeys(coords, np.zeros(num_points))
    return muscle_states


def calculate_current_muscle_states(model, muscle_name, coordinate_names, actuation=1.0):
    """Calculates the force, length and moment states of a muscle as a function of a
    coordinate.
    """
    # Init
    model.setAllControllersEnabled(True)
    state = model.getWorkingState()
    # disable_all_muscles(model, state)
    # enable_muscle(model, state, muscle_name)
    # Prepare the muscle
    muscle = model.getMuscles().get(muscle_name)
    muscle.setActivation(state, 1.0)
    model.assemble(state)
    model.equilibrateMuscles(state)
    model.realizeVelocity(state)
    # Result dict
    muscle_states = {}
    # Length
    muscle_states['fiber_length'] = muscle.getFiberLength(state)
    muscle_states['tendon_length'] = muscle.getTendonLength(state)
    muscle_states['muscle_tendon_length'] = muscle.getLength(state)
    muscle_states['pennation_angle'] = muscle.getPennationAngle(state)
    # Force
    muscle_states['active_fiber_force'] = muscle.getActiveFiberForce(state)
    muscle_states['passive_fiber_force'] = muscle.getPassiveFiberForce(state)
    muscle_states['tendon_force'] = muscle.getTendonForce(state)
    # Moment
    muscle_states['moment_arm'] = {
        coordinate:
        muscle.computeMomentArm(
            state, model.updCoordinateSet().get(coordinate))
        for coordinate in coordinate_names
    }
    muscle_states['moment'] = {
        coordinate:
        muscle_states['tendon_force'] * muscle_states['moment_arm'][coordinate]
        for coordinate in coordinate_names
    }
    return muscle_states


def calculate_muscle_states_for_coordinate_range(
        model, muscle_name, as_a_function_coordinate, coordinate_range,
        actuation=1.0
):
    '''Calculates the force, length and moment states of a muscle as a function of a
    coordinate.
    '''
    # Init
    coordinate = model.updCoordinateSet().get(as_a_function_coordinate)
    muscle = model.getMuscles().get(muscle_name)
    muscle_states = create_default_muscle_states_dict(
        len(coordinate_range), [as_a_function_coordinate]
    )
    muscle_states['joint_angles'][as_a_function_coordinate][:] = coordinate_range
    state = model.getWorkingState()
    for idx, value in enumerate(coordinate_range):
        coordinate.setValue(state, value)
        coordinate.setSpeedValue(state, 0.0)
        result = calculate_current_muscle_states(
            model, muscle_name, (as_a_function_coordinate,), actuation
        )
        for key, value in result.items():
            if "moment" in key:
                muscle_states[key][as_a_function_coordinate][idx] = value[as_a_function_coordinate]
            else:
                muscle_states[key][idx] = value
    return muscle_states


def export_muscle_data(osim_filepath, export_path, muscle_joint):
    """ export muscle data """
    # Create h5 data file
    data_file = h5py.File(export_path, 'w')
    # Initialize the model
    model, state = setup_osim_model(osim_filepath)
    # Extact muscle properties of all muscles in the model
    muscle_properties = extract_muscle_properties(model)
    # Extract coordinate properties
    coordinate_properties = extract_static_coordinate_properties(model)
    joints_range = {
        joint['name'] : joint['range']
        for coord_name, coord in coordinate_properties.items()
        for joint in coord
    }
    # Loop over muscle-joint
    for muscle, joint in tqdm(muscle_joint):
        # Reset model pose
        reset_model_to_zero_pose(model, state)
        # Create muscle group
        if muscle not in data_file.keys():
            muscle_group = data_file.create_group(name=muscle)
            # Add attributes Muscle properties
            for key,value in muscle_properties[muscle].items():
                muscle_group.attrs.create(key, value)
        else:
            muscle_group = data_file[muscle]
        # Create joint group
        joint_group = muscle_group.create_group(name=joint)
        # Get joint properties
        joint_range = joints_range[joint]
        # Calculate data
        muscle_states = calculate_muscle_states_for_coordinate_range(
            model, muscle, as_a_function_coordinate=joint,
            coordinate_range=np.linspace(joint_range[0], joint_range[1], 20),
            actuation=1.0
        )
        for key, value in muscle_states.items():
            if isinstance(value, dict):
                subgroup = joint_group.create_group(name=key)
                for subkey, subvalue in value.items():
                    subgroup.create_dataset(name=subkey, data=subvalue)
            else:
                joint_group.create_dataset(name=key, data=value)
