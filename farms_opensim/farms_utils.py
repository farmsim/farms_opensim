""" Utils functions for exporting to farms config file formats. """

import argparse
import os
import shutil
from pathlib import Path

import farms_pylog as pylog
import numpy as np
import trimesh as T
import vtk
import yaml
from farms_core.io.sdf import Collision, Joint, Link, ModelSDF, Visual
from farms_core.io.yaml import write_yaml
from farms_models import utils
from scipy.spatial.transform import Rotation

from farms_opensim import opensim_utils as osim_utils

pylog.set_level("error")


def setup_args_parser():
    """Setup the arg parser for reading osim and exporting to farms config

    Returns
    -------
    args : <ArgumentParser.args>
    """
    parser = argparse.ArgumentParser()

    parser.add_argument("--input", "-i", type=str,
                        required=True, dest="input",
                        help="Path to the osim file")

    parser.add_argument("--output", "-o", type=str,
                        required=False, dest="output",
                        help="Path to the output folder")

    parser.add_argument("--geometry-path", "-gp", type=str, nargs='+',
                        required=False, dest="geometry_path", default=None,
                        help="Path to the folder containing geometries in stl format")

    parser.add_argument("--export-sdf-path", "-esp", type=str,
                        required=False, dest="sdf_path", default=None,
                        help="Option to export sdf config to a non-standard path")

    parser.add_argument("--export-muscles", "-m", type=bool,
                        required=False, dest="muscles", default=False,
                        help="Option to generate muscles config or not")

    parser.add_argument("--export-muscles-path", "-emp", type=str,
                        required=False, dest="muscles_path", default=None,
                        help="Option to export muscles config to a non-standard path")

    parser.add_argument("--farms-name", "-fn", type=str,
                        required=True, dest="farms_name",
                        help="Farms model name to save as")

    parser.add_argument("--farms-version", "-fv", type=str,
                        required=True, dest="farms_version",
                        help="Farms model version")

    parser.add_argument("--author", "-a", type=str,
                        required=False, dest="author", default="unknown",
                        nargs='+', help="Author of the model")

    parser.add_argument("--email", "-e", type=str,
                        required=False, dest="email", default="unknown",
                        help="Contact email for the model")

    args = parser.parse_args()

    return args


def generate_farms_animal_folder(path, name, version, **kwargs):
    """
    Generate the folders necessary for configuring and generating animal models
    in farms.

    Parameters
    ----------
    path: <str>
        Path to the animal folder to generate the farms config files.

    Return
    ------
    out: <None>
        None
    """

    # Folders necessary
    # 1. config
    # 2. meshes
    # 3. sdf

    # Create the main directory
    utils.create_new_model(path, name, version, **kwargs)

    # Generate directories if they are not present
    try:
        os.mkdir(os.path.join(path, 'config'))
    except FileExistsError:
        pylog.warning("config folder already exists in the path provided."
                      "This will overwrite the existing files in that folder")
    try:
        os.mkdir(os.path.join(path, 'meshes'))
    except FileExistsError:
        pylog.warning("meshes folder already exists in the path provided."
                      "This will overwrite the existing files in that folder")
    try:
        os.mkdir(os.path.join(path, 'sdf'))
    except FileExistsError:
        pylog.warning("sdf folder already exists in the path provided."
                      "This will overwrite the existing files in that folder")


def generate_muscle_config(model):
    """Get the muscle config data structure

    Parameters
    ----------
    model : <Opensim.Model>
        Opensim Model

    Returns
    -------
    out : <dict>
        Dictionary of muscle properties necessary for farms config
    """

    # Extract muscle properties
    muscle_properties = osim_utils.extract_muscle_properties(model)
    muscle_path = osim_utils.extract_muscle_path_point_properties(model)

    # Farms-Muscle config file
    farms_muscle_config = {}

    for muscle, prop in muscle_properties.items():
        farms_muscle_config[muscle] = {}
        # [TO-DO] : In future try to read from the osim model as we will
        # support all models
        farms_muscle_config[muscle]['model'] = 'millard_rt'
        farms_muscle_config[muscle]['name'] = muscle
        farms_muscle_config[muscle]['l_opt'] = prop['optimal_fiber_length']
        farms_muscle_config[muscle]['l_slack'] = prop['tendon_slack_length']
        farms_muscle_config[muscle]['pennation'] = prop['pennation_angle']
        farms_muscle_config[muscle]['f_max'] = prop['max_isometric_force']
        farms_muscle_config[muscle]['v_max'] = prop['max_contraction_velocity']
        farms_muscle_config[muscle]['l_ce0'] = prop['optimal_fiber_length']
        farms_muscle_config[muscle]['a0'] = 0.05
        farms_muscle_config[muscle]['visualize'] = True
        farms_muscle_config[muscle]['waypoints'] = []

    for muscle, data in muscle_path.items():
        for point in data:
            try:
                ppoint = point['location']
                _path_point = [{'link': point['body']},
                               {'point': ppoint}]
            except KeyError:
                pylog.warning("Skipping moving point {}".format(point))
                continue
            farms_muscle_config[muscle]['waypoints'].append(_path_point)
            pylog.info(
                "Muscle {} -> Body {} -> Location {}".format(
                    muscle, point['body'], point['location']))

    farms_muscle_config = {'muscles': farms_muscle_config}
    return farms_muscle_config


def vtp_stl_converter(vtp_file_path, export_path):
    """
    Convert a vtp file to stl with the same name.

    Parameters
    ----------
    vtp_file_path: <str>
        File path to the vtp file

    export_path: <str>
        Folder path to export the converted stl file

    Return
    ------
    out: <None>
        None
    """
    reader = vtk.vtkXMLPolyDataReader()
    reader.SetFileName(vtp_file_path)
    writer = vtk.vtkXMLPolyDataWriter()
    writer = vtk.vtkSTLWriter()
    file_name = os.path.split(vtp_file_path)[1]
    stl_file_path = os.path.join(
        export_path, file_name.replace("vtp", "stl")
    )
    writer.SetFileName(stl_file_path)
    writer.SetInputConnection(reader.GetOutputPort())
    writer.Write()


def search_geometry_file(file_name):
    """ Method to search for the geometry file. """
    for folder in osim_utils.get_opensim_geometry_paths():
        if file_name in os.listdir(folder):
            return folder
    return None


def export_meshes(model, export_path):
    """ Write meshes to the meshes folder. """

    # Create temp directory to store the meshes
    mesh_path = os.path.join(export_path, "meshes")
    mesh_temp_path = os.path.join(mesh_path, "temp")

    os.mkdir(mesh_temp_path)

    # Extract body properties
    body_properties = osim_utils.extract_static_body_properties(model)

    for _, body in body_properties.items():
        for geometry in body['geometries']:
            file_name = Path(geometry['file']).stem
            file_ext = Path(geometry['file']).suffix
            print(file_name, file_ext)
            # Convert if VTP file
            if file_ext.lower() == ".vtp":
                vtp_file = os.path.join(
                    search_geometry_file(geometry['file']),
                    geometry['file']
                )
                vtp_stl_converter(vtp_file, mesh_temp_path)
                # Reload model to export it to obj file
                mesh = T.load(
                    os.path.join(
                        mesh_temp_path, file_name+".stl"
                    )
                )
            elif (file_ext.lower() == ".obj") or (file_ext.lower() == ".stl"):
                mesh = T.load(
                    os.path.join(
                        search_geometry_file(geometry['file']),
                        geometry['file']
                    )
                )
            # Scaling
            scaling_matrix = T.transformations.scale_matrix(
                geometry['scale'][0], direction=[1, 0, 0]
            )@T.transformations.scale_matrix(
                geometry['scale'][1], direction=[0, 1, 0]
            )@T.transformations.scale_matrix(
                geometry['scale'][2], direction=[0, 0, 1]
            )
            mesh.apply_transform(scaling_matrix)

            # Export mesh
            pylog.debug(
                "Writing mesh {} to {}".format(file_name, mesh_path)
            )
            mesh.export(os.path.join(mesh_path, file_name+".obj"))

    # Delete temp file
    shutil.rmtree(mesh_temp_path)


def convert_custom_joint(
        links, joints, joint_name, joint_data, coordinates, joint_transforms
):
    """

    Parameters
    ----------
    links :

    joints :

    coordinates :


    Returns
    -------
    out :

    """
    # Create 2 interim dummy links
    parent = joint_data['parent']
    child = joint_data['child']
    n_inter_links = len(coordinates)-1
    for nlink in range(n_inter_links):
        links[child+f'_interim_{nlink}'] = Link.empty(
            child+f'_interim_{nlink}', links[child].pose
        )

    parent_child = [
        [parent, f"{child}_interim_0"],
        *[
            [f"{child}_interim_{nlink}", f"{child}_interim_{nlink+1}"]
            for nlink in range(0, n_inter_links-1)
        ],
        [f"{child}_interim_{n_inter_links-1}", child]
    ]
    for j, coordinate in enumerate(coordinates):
        joints[coordinate['name']] = Joint(
            name=coordinate['name'],
            joint_type='prismatic' if coordinate['motion_type'] == 2 else 'revolute',
            parent=links[parent_child[j][0]], child=links[parent_child[j][1]],
            pose=np.zeros((6,)),
            # pose=sdf_pose(
            #     joint_transforms[joint_name]['location'],
            #     joint_transforms[joint_name]['orientation']
            # ) if j==1 else np.zeros((6,)),
            xyz=coordinate['axis'],
            limits=[*coordinate['range'], -1.0, -1.0]
        )


def sdf_pose(location, rotation):
    """ Convert location, rotation to sdf pose list format """
    return [
        *location,
        *((Rotation.from_matrix(rotation)).as_euler('xyz').tolist())
    ]


def organize_inertia(inertia):
    """ Organize inertia tensor to sdf format from opensim. """
    return [
        inertia[0], inertia[3], inertia[4],
        inertia[1], inertia[5], inertia[2]
    ]


def generate_sdf(model, state, **kwargs):
    """Convert to sdf from opensim osim format

    Returns
    -------
    out :

    """
    geometry_path = kwargs.get('geometry_path', None)
    osim_file = kwargs.get('input')
    output_path = kwargs.get('output')
    sdf_path = kwargs.get('sdf_path')

    print(sdf_path)

    if geometry_path:
        for path in geometry_path:
            osim_utils.add_opensim_geometry_path(path)

    # Export meshes
    if output_path:
        export_meshes(model, output_path)
    elif sdf_path:
        # mesh_export_path = os.path.join(sdf_path, 'meshes')
        export_meshes(model, sdf_path)
    else:
        pylog.error("Need to provide either output or sdf_export_path")
        raise ValueError

    # Generate links and joints
    links = {}
    # Generate links
    for link_name, data in osim_utils.calculate_absolute_body_transformations(
            model, state
    ).items():
        links[link_name] = Link.empty(
            name=link_name,
            pose=sdf_pose(data['location'], data['orientation'])
        )
    # Add dynamics, visual, collisions
    mesh_uri = "../meshes/{}.obj" if output_path else "./meshes/{}.obj"
    for j, (link_name, data) in enumerate(osim_utils.extract_static_body_properties(model).items()):
        link = links[link_name]
        # Dynamics
        inertial = link.inertial
        inertial.mass = data['mass']
        inertial.pose = [*data['CoM'], *[0.0]*3]
        inertial.inertias = organize_inertia(data['inertia'])
        # Visual and Collision
        for geometry in data['geometries']:
            name = geometry['file'].split('.')[0]
            link.visuals.append(Visual.from_mesh(
                name=name,
                mesh=mesh_uri.format(name),
                scale=geometry['scale'],
                color=[(j+2) % 2, 0, (j+2) % 3, 1]
            ))
            link.visuals.append(Collision.from_mesh(
                name=name,
                mesh=mesh_uri.format(name),
                scale=geometry['scale']
            ))

    # Generate Joints
    joints = {}
    joint_transforms = osim_utils.calculate_joint_transformations(model, state)
    for (joint_name, data), coordinates in zip(
            osim_utils.extract_static_joint_properties(model).items(),
            osim_utils.extract_static_coordinate_properties(model).values()
    ):
        if data['parent'] == 'ground':
            continue
        # For multiple coordinates we will introduce dummy links
        if len(coordinates) >= 2:
            # Dimitar: sometimes pelvis could be defined as Rx, tx, ty
            # so should not be a ball joint
            convert_custom_joint(
                links, joints, joint_name, data, coordinates, joint_transforms
            )
        elif len(coordinates) == 1:
            coordinate = coordinates[0]
            joint_type = {-1: 'fixed', 1: 'revolute', 2: 'prismatic'}
            joints[coordinate['name']] = Joint(
                name=coordinate['name'],
                joint_type=joint_type[coordinate['motion_type']],
                parent=links[data['parent']], child=links[data['child']],
                pose=np.zeros((6,)),
                # pose=sdf_pose(
                #     joint_transforms[joint_name]['location'],
                #     joint_transforms[joint_name]['orientation']
                # ),
                xyz=coordinate['axis'],
                limits=[*coordinate['range'], -1.0, -1.0]
            )
        else:
            pylog.debug(
                "coordinate pass {}, {}, {}".format(joint_name, data, coordinates)
            )
            pass
    # setup sdf model
    sdf_model = ModelSDF(name=model.getName(), pose=[0.0]*6)
    sdf_model.links = list(links.values())
    sdf_model.joints = list(joints.values())

    return sdf_model


def convert_to_farms():
    """Convert the opensim model to farms.

    Parameters
    ----------
    **kwargs :


    Returns
    -------
    out :

    """
    # Setup argument parsing
    args = setup_args_parser()

    kwargs = vars(args)

    # Generate farms animal folder
    if kwargs['output']:
        generate_farms_animal_folder(
            kwargs['output'], name=kwargs['farms_name'],
            version=kwargs['farms_version'], author=kwargs['author'],
            email=kwargs['email']
        )

    # Setup opensim model
    model_osim, state_osim = osim_utils.setup_osim_model(kwargs['input'])

    # Export osim
    if kwargs['output']:
        model_osim.printToXML(
            os.path.join(kwargs['output'], "config",f"{model_osim.getName()}.osim")
        )

    # Generate sdf
    model_sdf = generate_sdf(model_osim, state_osim, **vars(args))
    # Export sdf
    sdf_name = "{}.sdf".format(kwargs['farms_name'])
    sdf_path = (
        os.path.join(kwargs['output'], "sdf", sdf_name)
        if kwargs['output']
        else
        os.path.join(kwargs['sdf_path'], sdf_name)
    )
    model_sdf.write(sdf_path)

    # Generate and export muscles config
    muscles_path = (
        os.path.join(kwargs['output'], "config", "muscles.yaml")
        if kwargs['output']
        else
        os.path.join(kwargs['muscles_path'], "muscles.yaml")
    )
    write_yaml(
        data=generate_muscle_config(model_osim),
        file_path=muscles_path
    )


if __name__ == '__main__':
    # Setup argument parsing
    args = setup_args_parser()

    # Generate
    generate_sdf(**vars(args))
