from setuptools import setup, dist, find_packages

setup(
    name='farms_opensim',
    version='0.1',
    description='Package containing generation of mouse models using blender',
    url='https://gitlab.com/FARMSIM/farms_opensim.git',
    author='biorob-farms',
    author_email='biorob-farms@groupes.epfl.ch',
    license='MIT',
    packages=find_packages(),
    install_requires=[
        'PyYaml', 'trimesh', 'vtk', 'scipy',
        'farms_pylog',
        'farms_core',
        'farms_models',
    ],
    zip_safe=False,
    entry_points={
        'console_scripts': [
            'osim-to-farms-converter=farms_opensim.farms_utils:convert_to_farms'
        ],
    }

)
