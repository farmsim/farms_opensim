""" Test script for comparing muscle plots from the gui"""

import os

import matplotlib.pyplot as plt
import numpy as np
import opensim

from farms_opensim.opensim_utils import \
    calculate_muscle_states_for_coordinate_range

# %%

model_file = os.path.abspath(
    '../opensim-models/Models/Gait2392_Simbody/gait2392_simbody.osim'
)

model = opensim.Model(model_file)
model.initSystem()

muscle = 'soleus_r'
as_a_function_coordinate = 'ankle_angle_r'
coordinate_range = np.linspace(np.deg2rad(-75), np.deg2rad(75), 20)
muscle_states = calculate_muscle_states_for_coordinate_range(
    model, muscle, as_a_function_coordinate, coordinate_range
)

#: Load results from GUI
gui_tendon_length = np.loadtxt("./soleus_r_ankle_angle_r", skiprows=7)
gui_fiber_length = np.loadtxt("./fiber_length_ankle_r", skiprows=7)

plt.figure()
plt.plot(np.rad2deg(coordinate_range), muscle_states["tendon_length"])
plt.plot(gui_tendon_length[:, 1], gui_tendon_length[:, 2])
plt.xlabel('coordinate (deg)')
plt.ylabel('Tendon length(m)')
plt.legend(['Script', 'GUI'])
plt.title('Tendon length of ' + muscle + ' as function of ' +
          as_a_function_coordinate)
plt.grid(True)

plt.figure()
plt.plot(np.rad2deg(coordinate_range), muscle_states["fiber_length"])
plt.plot(gui_fiber_length[:, 1], gui_fiber_length[:, 2])
plt.xlabel('coordinate (deg)')
plt.ylabel('Fiber length(m)')
plt.legend(['Script', 'GUI'])
plt.title('Fiber length of ' + muscle + ' as function of ' +
          as_a_function_coordinate)
plt.grid(True)
plt.show()
