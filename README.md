# OpenSim Farms

An OpenSim to farms model exporter.

## Installation

Make sure to clone the submodules of this project:

`git clone --recurse-submodules <URL>`

Install OpenSim v4.0 with Python bindings. For convenience a pre-build
version of OpenSim for Ubuntu 18.04 can be downloaded through the
following URL:

https://sourceforge.net/projects/dependencies/files/opensim-core/opensim-core-4.0-ubunut18.04.tar.xz

Extract and move the opensim-core folder to a convenient
location. Then configure the environmental variables as follows:

`export OPENSIM_HOME=/path-to-filesystem/opensim-core`

`export OpenSim_DIR=$OPENSIM_HOME/lib/cmake/OpenSim`

`export LD_LIBRARY_PATH=$OPENSIM_HOME/lib:$LD_LIBRARY_PATH`

`export PATH=$OPENSIM_HOME/bin:$PATH`

`export PATH=$OPENSIM_HOME/libexec/simbody:$PATH`

The Python bindings where configured with Python 3.6. To install run: 

`cd $OPENSIM_HOME/lib/python3.6/site-packages`

`python3 setup.py install --user`

To test the Python bindings try importing OpenSim in python3:

`import opensim`

For OpenSim to locate the geometry folder one must create a symbolic link in the
OPENSIM_HOME directory:

`git clone https://github.com/opensim-org/opensim-models.git`

`ln -s /pathto-filesystem/opensim-models/Geometry/ $OPENSIM_HOME`


