# Demonstrates how to compute muscle related quantities (e.g., moment
# arm, muscle fiber length, etc.).
#
# author: Dimitar Stanev (dimitar.stanev@epfl.ch)
# %%
import opensim
import os
import numpy as np
import matplotlib.pyplot as plt

# %%

model_file = os.path.abspath(
    '../opensim-models/Models/Gait2392_Simbody/subject01.osim')

model = opensim.Model(model_file)
model.initSystem()

# %%
# example moment arm


def calculate_moment_arm(muscle_name, at_coordinate,
                         as_a_function_coordinate, coordinate_range):
    '''Calculates the moment arm of a muscle at a specific coordinate as a
    function of a coordinate. Coordinate can be different
    (bi-articular muscles) or the same for mono-articular muscles.

    '''
    state = model.getWorkingState()
    coordinate = model.updCoordinateSet().get(as_a_function_coordinate)
    muscle = model.getMuscles().get(muscle_name)
    at_coordinate = model.getCoordinateSet().get(at_coordinate)
    moment_arm = []
    for value in coordinate_range:
        coordinate.setValue(state, value)
        model.realizePosition(state)
        moment_arm.append(muscle.computeMomentArm(state, at_coordinate))

    return moment_arm


muscle = 'soleus_r'
at_coordinate = 'ankle_angle_r'
as_a_function_coordinate = 'ankle_angle_r'
coordinate_range = np.linspace(np.deg2rad(-60), np.deg2rad(60), 20)
moment_arm = calculate_moment_arm(muscle, at_coordinate,
                                  as_a_function_coordinate, coordinate_range)

plt.figure()
plt.plot(np.rad2deg(coordinate_range), moment_arm)
plt.xlabel('coordinate (deg)')
plt.ylabel('moment arm (m)')
plt.title('Moment arm of ' + muscle + ' at ' + at_coordinate + ' as function of '
          + as_a_function_coordinate)

# %%
# example fiber-length (similar for tendon length and muscle-tendon
# length)
#
# https://simtk.org/api_docs/opensim/api_docs/classOpenSim_1_1Muscle.html


def calculate_fiber_length(muscle_name, as_a_function_coordinate,
                           coordinate_range):
    '''Calculates the fiber-length of a muscle as a function of a
    coordinate.

    '''
    state = model.getWorkingState()
    coordinate = model.updCoordinateSet().get(as_a_function_coordinate)
    muscle = model.getMuscles().get(muscle_name)
    fiber_length = []
    for value in coordinate_range:
        coordinate.setValue(state, value)
        model.equilibrateMuscles(state)
        fiber_length.append(muscle.getFiberLength(state))

    return fiber_length


muscle = 'soleus_r'
as_a_function_coordinate = 'ankle_angle_r'
coordinate_range = np.linspace(np.deg2rad(-60), np.deg2rad(60), 20)
fiber_length = calculate_fiber_length(muscle, as_a_function_coordinate,
                                      coordinate_range)

plt.figure()
plt.plot(np.rad2deg(coordinate_range), fiber_length)
plt.xlabel('coordinate (deg)')
plt.ylabel('fiber length (m)')
plt.title('Fiber length of ' + muscle + ' as function of ' +
          as_a_function_coordinate)

# %%
# example active fiber-force (same for passive or tendon force)


def calculate_active_fiber_force(muscle_name, actuation,
                                 as_a_function_coordinate,
                                 coordinate_range):
    '''Calculates the active fiber-force of a muscle as a function of a
    coordinate assuming a constant actuation.

    '''
    state = model.getWorkingState()
    coordinate = model.updCoordinateSet().get(as_a_function_coordinate)
    muscle = model.getMuscles().get(muscle_name)
    muscle.overrideActuation(state, True)
    active_fiber_force = []
    for value in coordinate_range:
        coordinate.setValue(state, value)
        muscle.setOverrideActuation(state, actuation)
        model.equilibrateMuscles(state)
        active_fiber_force.append(muscle.getActiveFiberForce(state))

    return active_fiber_force


muscle = 'soleus_r'
as_a_function_coordinate = 'ankle_angle_r'
coordinate_range = np.linspace(np.deg2rad(-60), np.deg2rad(60), 20)
active_fiber_force = calculate_active_fiber_force(muscle, 1.0,
                                                  as_a_function_coordinate,
                                                  coordinate_range)

plt.figure()
plt.plot(np.rad2deg(coordinate_range), active_fiber_force)
plt.xlabel('coordinate (deg)')
plt.ylabel('active fiber force (N)')
plt.title('Active fiber force of ' + muscle + ' as function of ' +
          as_a_function_coordinate)

# %%
