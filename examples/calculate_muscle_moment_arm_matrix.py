# Demonstrates how to compute the muscle moment arm matrix and
# multiply this by the muscle forces to get joint moments.
#
# author: Dimitar Stanev (dimitar.stanev@epfl.ch)
# %%
import opensim
import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# %%
# functions


def to_gait_cycle(data_frame, t0, tf):
    temp = data_frame[(data_frame.time >= t0) & (data_frame.time <= tf)].copy()
    temp.time = data_frame['time'].transform(lambda x: 100.0 / (tf - t0) * (x - t0))
    return temp


def osim_array_to_list(array):
    """Convert OpenSim::Array<T> to Python list.
    """
    temp = []
    for i in range(array.getSize()):
        temp.append(array.get(i))

    return temp


def read_from_storage(model_file, file_name, sampling_interval):
    """Read OpenSim.Storage files.

    Parameters
    ----------
    file_name: (string) path to file

    Returns
    -------
    tuple: (labels, time, data)

    """
    sto = opensim.Storage(file_name)
    if sto.isInDegrees():
        model = opensim.Model(model_file)
        model.initSystem()
        model.getSimbodyEngine().convertDegreesToRadians(sto)

    sto.resampleLinear(sampling_interval)

    labels = osim_array_to_list(sto.getColumnLabels())
    time = opensim.ArrayDouble()
    sto.getTimeColumn(time)
    time = osim_array_to_list(time)
    data = []
    for i in range(sto.getSize()):
        temp = osim_array_to_list(sto.getStateVector(i).getData())
        temp.insert(0, time[i])
        data.append(temp)

    df = pd.DataFrame(data, columns=labels)
    df.index = df.time
    return df


def calculate_moment_arm_matrix(osim_model, pose):
    '''Calculates the moment arm matrix given a pose.
    '''
    # update coordinate values
    state = model.getWorkingState()
    for coordinate_name, value in pose.items():
        model.updCoordinateSet().get(coordinate_name).setValue(state, value)

    model.realizePosition(state)

    # calculate moment arm matrix
    moment_arm = []
    for i in range(model.getCoordinateSet().getSize()):
        muscle_force = []
        coordinate = model.getCoordinateSet().get(i)
        for j in range(model.getMuscles().getSize()):
            muscle_force.append(model.getMuscles().get(j)\
                                .computeMomentArm(state, coordinate))

        moment_arm.append(muscle_force)

    return np.array(moment_arm)


def get_muscle_names(osim_model):
    muscle_names = []
    for i in range(osim_model.getMuscles().getSize()):
        muscle_names.append(osim_model.getMuscles().get(i).getName())

    return muscle_names


def get_coordinate_names(osim_model):
    coordinate_names = []
    for i in range(osim_model.getCoordinateSet().getSize()):
        coordinate_names.append(osim_model.getCoordinateSet().get(i).getName())

    return coordinate_names


# %%
# load and prepare files

# some of the files do not exist in the OutputReference model of
# opensim-models, but they were added for testing (bug in
# opensim-models)
model_file = os.path.abspath(
    '../opensim-models/Models/Gait2392_Simbody/subject01.osim')
kinematics_file = os.path.abspath(
    '../opensim-models/Pipelines/Gait2392_Simbody/OutputReference/InverseKinematics/subject01_walk1_ik.mot')
muscle_forces_file = os.path.abspath(
    '../opensim-models/Pipelines/Gait2392_Simbody/OutputReference/StaticOptimization/subject01_StaticOptimization_force.sto')
torques_file = os.path.abspath(
    '../opensim-models/Pipelines/Gait2392_Simbody/OutputReference/ResultsInverseDynamics/subject01_inverse_dynamics.sto')

# initialize OpenSim model
model = opensim.Model(model_file)
model.initSystem()

# load files
kinematics = read_from_storage(model, kinematics_file, 0.01)
muscle_forces = read_from_storage(model, muscle_forces_file, 0.01)
torques = read_from_storage(model, torques_file, 0.01)

# analyze only a single gait cycle
t0 = 0.6
tf = 1.83
kinematics = to_gait_cycle(kinematics, t0, tf)
muscle_forces = to_gait_cycle(muscle_forces, t0, tf)
torques = to_gait_cycle(torques, t0, tf)

# get muscle and coordinate order in model (order is important)
muscle_names = get_muscle_names(model)
coordinate_names = get_coordinate_names(model)

# remove _moment or _force so that we can index the data using
# coordinate_names order
tau_columns = torques.columns
torques.columns = [tau_columns[i].replace('_moment', '').replace('_force', '')
                   for i in range(tau_columns.shape[0])]

# %%
# calculate residuals between desired and muscle induced torque

residuals = []
for i in range(muscle_forces.shape[0]):
    q = kinematics.iloc[i][coordinate_names]
    fm = muscle_forces.iloc[i][muscle_names]
    tau = torques.iloc[i][coordinate_names]
    R = calculate_moment_arm_matrix(model, q)
    residuals.append(np.subtract(tau, np.dot(R, fm)))


residuals = np.array(residuals)

# %%
# plot

plt.figure()
plt.plot(muscle_forces.time, residuals[:, 0:6])
plt.savefig('pelvis_residuals.pdf')
# plt.show()

# %%
