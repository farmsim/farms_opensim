# convert gait0918
osim-to-farms-converter -i ../opensim-models/Models/Gait10dof18musc/gait10dof18musc.osim -o ./gait0918/ -gp ../opensim-models/Geometry/ -fn gait0918 -fv 1.0

# convert gait2392
osim-to-farms-converter -i ../opensim-models/Models/Gait2392_Simbody/gait2392_millard2012muscle.osim -o ./gait2392/ -gp ../opensim-models/Geometry/ -fn gait2392 -fv 1.0

