##
# OpenSim to farms model exporter main script. This was tested with OpenSim
# v4.0.
#
# author: Dimitar Stanev (dimitar.stanev@epfl.ch)
import sys
sys.path.append('../')
import os
import opensim
from farms_opensim.opensim_utils import *
import yaml
import pprint
import argparse
import numpy as np
import farms_pylog as pylog


# # ARGPARSE
# parser = argparse.ArgumentParser()

# parser.add_argument("--input", "-i", type=str,
#                     required=True, dest="INPUT",
#                     help="Path to the osim file")

# parser.add_argument("--output", "-o", type=str,
#                     required=False, dest="OUTPUT", default=None,
#                     help="Path to the output folder")

# args = parser.parse_args()

# initialization
# model_file = os.path.abspath(args.INPUT)

model_file = os.path.abspath(
    '../opensim-models/Models/Gait2392_Simbody/subject01.osim')

model = opensim.Model(model_file)
# model.printToXML('subject01.osim')
state = model.initSystem()

# #: Set initial state to zero
# coordinate_set = model.getCoordinateSet()
# coordinate_names = opensim.ArrayStr()
# coordinate_set.getNames(coordinate_names)

# for coord_index in range(coordinate_names.getSize()):
#     coordinate = coordinate_set.get(coord_index)
#     print(coordinate.getName(),
#           np.rad2deg(coordinate.get_range(0)),
#           np.rad2deg(coordinate.get_range(1)))
#     if coordinate.get_range(0) > 0.0:
#         pylog.debug("Setting coordinate values {}".format(np.rad2deg(coordinate.get_range(0))))
#         coordinate.setValue(state, coordinate.get_range(0))        
#     elif coordinate.get_range(1) < 0.0:
#         pylog.debug("Setting coordinate values {}".format(np.rad2deg(coordinate.get_range(1))))
#         coordinate.setValue(state, coordinate.get_range(1))
#     else:
#         coordinate.setValue(state, 0.0)

##
# functionality

body_properties = extract_static_body_properties(model)
# pprint.pprint(body_properties)

# relative coordinates
joint_properties = extract_static_joint_properties(model)
joint_transformations = calculate_joint_transformations(model, state)
coordinate_properties = extract_static_coordinate_properties(model)

# absolute body transformations for the current pose
absolute_body_transformations = calculate_absolute_body_transformations(model,
                                                                        state)
# get absolute muscle path point locations for the current pose
absolute_muscle_path_points = calculate_absolute_muscle_path_point(model, state)

##

# # export to yaml
# kinematic_skeleton = {}
# _bt = absolute_body_transformations
# for joint, prop in joint_properties.items():
#     child = prop['child']
#     if 'ground' in prop['parent']:
#         parent = None
#     else:
#         parent = prop['parent']
#     kinematic_skeleton[child] = {'origin': _bt[child]['location'],
#                                  'parent': parent}

# # Dump yaml file
# if args.OUTPUT:
#     with open(os.path.join(args.OUTPUT, 'kinematic_skeleton.yaml'), 'w') as file:
#         yaml.dump(kinematic_skeleton, file, default_flow_style=False)

# muscle static properties
muscle_path = extract_muscle_path_point_properties(model)
muscle_properties = extract_muscle_properties(model)

# #: Farms-Muscle config file
# farms_muscle_config = {}

# for muscle, property in muscle_properties.items():
#     farms_muscle_config[muscle] = {}
#     farms_muscle_config[muscle]['model'] = 'geyer'
#     farms_muscle_config[muscle]['name'] = muscle
#     farms_muscle_config[muscle]['l_opt'] = property['optimal_fiber_length']
#     farms_muscle_config[muscle]['l_slack'] = property['tendon_slack_length']
#     farms_muscle_config[muscle]['pennation'] = property['pennation_angle']
#     farms_muscle_config[muscle]['f_max'] = property['max_isometric_force']
#     farms_muscle_config[muscle]['v_max'] = property['max_contraction_velocity']
#     farms_muscle_config[muscle]['l_ce0'] = property['optimal_fiber_length']
#     farms_muscle_config[muscle]['a0'] = 0.05
#     farms_muscle_config[muscle]['visualize'] = True
#     farms_muscle_config[muscle]['waypoints'] = []


# for muscle, data in muscle_path.items():
#     pprint.pprint(data)
#     for point in data:
#         _path_point = [{'link': point['Body']},
#                        {'point': point['Location']}]
#         farms_muscle_config[muscle]['waypoints'].append(_path_point)
#         pylog.info(
#             "Muscle {} -> Body {} -> Location {}".format(
#                 muscle, point['body'], point['location']))

# farms_muscle_config = {'muscles': farms_muscle_config}

# # Dump yaml file
# if args.OUTPUT:
#     with open(os.path.join(args.OUTPUT, 'muscles.yaml'), 'w') as file:
#         yaml.dump(farms_muscle_config, file, default_flow_style=None,
#                   explicit_start=True, indent=4, width=80)
# # dump yaml file
# path = './test_subject01.yaml'

# with open(path, 'w') as file:
#     yaml.dump(kinematic_skeleton, file, default_flow_style=False)


##
