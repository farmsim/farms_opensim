# A script for visualizing sdf models through pybullet.
import pybullet as p
import time
import numpy as np
import matplotlib.pyplot as plt
from argparse import ArgumentParser
plt.rcParams.update({'font.size': 14})
plt.rc('font', family='serif')
plt.rc('xtick', labelsize='x-small')
plt.rc('ytick', labelsize='x-small')


def create_floor(pybullet, position=None):
    """

    Parameters
    ----------
    pybullet :


    Returns
    -------
    out :

    """
    #: Create a plane
    position = position if position else (0., 0., 0.)
    plane = pybullet.createCollisionShape(pybullet.GEOM_PLANE)
    floor = pybullet.createMultiBody(
        0, plane, basePosition=position
    )
    return floor


def load_model(pybullet, filepath):
    """

    Parameters
    ----------
    filepath :

    Returns
    -------
    out :

    """
    #: LOAD SDF
    model_id = pybullet.loadSDF(sdfFileName=filepath)[0]
    return model_id


def main(filepath):
    #: Connect to GUI
    p.connect(p.GUI)

    #: Create floor
    floor_id = create_floor(p, position=[0., 0., -1.])

    #: Load model
    model_id = load_model(
        p,
        filepath=filepath
    )

    #: Setup simulation
    p.setGravity(0, 0, -10)
    p.setRealTimeSimulation(0)
    dt = 1. / 1000.
    run_time = 30.  # : seconds
    time_steps = np.linspace(0, run_time, int(run_time/dt))
    #: Disable velocity and position controllers
    p.setJointMotorControlArray(
        bodyUniqueId=model_id,
        jointIndices=np.arange(p.getNumJoints(model_id)),
        controlMode=p.VELOCITY_CONTROL,
        targetVelocities=np.arange(p.getNumJoints(model_id))*0.0,
        forces=np.arange(p.getNumJoints(model_id))*0.0
    )
    #: Enable torque sensor
    p.enableJointForceTorqueSensor(model_id, 0)
    #: Remove damping
    p.changeDynamics(
        model_id, 0, linearDamping=0.0, angularDamping=0.0,
        jointDamping=.0
    )

    #: Main loop
    for j, _ in enumerate(time_steps):
        joint_state = p.getJointState(model_id, 0)
        p.setJointMotorControl2(
            model_id,
            0,
            p.TORQUE_CONTROL,
            force=0.0
        )
        #: step simulation
        p.stepSimulation()
        #: Real time
        time.sleep(0.001)


if __name__ == '__main__':
    parser = ArgumentParser("SDF")
    parser.add_argument("--filepath", "-f", required=True,
                        dest="filepath", type=str)
    args = parser.parse_args()
    main(args.filepath)
